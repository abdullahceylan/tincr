$.ajaxSettings.accepts.text = "*/*"
var projectManager = new ProjectManager();
chrome.extension.onMessage.addListener(function(message, sender, sendResponse) {


    if (message.key == 'ProjectTypes'){
        //var cleanResponse = [];
        //for (var i = 0; i < ProjectTypes.length){
        //  cleanResponse.push({name: ProjectTypes[i].name, locationType: ProjectTypes[i].locationType});
        //}
        sendResponse(ProjectTypes);
    }
    else if (message.key == 'launchFolderSelect'){
        projectManager.launchFolderSelect(message.tabId, message.url, message.index, sendResponse);
    }
    else if (message.key == 'launchFileSelect'){
        projectManager.launchFileSelect(message.tabId, message.url, message.op, sendResponse);
    }
    else if (message.key == 'checkResources'){
        projectManager.checkResources(message.tabId, message.resources, sendResponse);
    }
    else if (message.key == 'checkResourceContent'){
        projectManager.checkResourceContent(message.tabId, message.url, message.content, sendResponse);
    }
    else if (message.key == 'updateResource'){
        projectManager.updateResource(message.tabId, message.url, message.content, sendResponse);
    }
    else if (message.key == 'pageChanged'){
        projectManager.resetProject(message.tabId, sendResponse);
    }
    else if (message.key == 'loadProject'){
        projectManager.loadProject(message.tabId, message.type, message.path, message.url, message.urlPathMap, sendResponse);
    }
    else if (message.key == 'unwatchDirectory'){
        projectManager.unwatchDirectory(message.tabId, sendResponse);
    }
    else if (message.key == 'setResourceOptions'){
        projectManager.setResourceOptions(message.tabId, message.url, message.exactMatch, sendResponse);
    }
    else if (message.key == 'removeDependency'){
        projectManager.removeDependency(message.tabId, message.url, Number(message.index), sendResponse);
    }
    else if (message.key == 'setCustomPathData'){
        projectManager.setCustomPathData(message.tabId, message.url, message.data, sendResponse);
    }
    else if (message.key == 'clearResource'){
        projectManager.clearResource(message.tabId, message.url, sendResponse);
    }
    else if (message.key == 'getPreference'){
        chrome.storage.local.get(message.prefKey, sendResponse);
    }
    else if (message.key == 'setPreference'){
        chrome.storage.local.set(message.prefObj, sendResponse);
    }
    else{
        sendResponse({});
    }

  return true;

});
chrome.extension.onConnect.addListener(function(port){
    projectManager.watchDirectory(port);
});
chrome.tabs.onRemoved.addListener(function(tabId, removeInfo) {
    projectManager.cleanUp(tabId);
});