window.nativeFileSupport = document.getElementById("pluginId");

var checkNativeFileSupport = function(){
    try{
        console.log(nativeFileSupport.echo('Native file support successfully loaded'));
    }
    catch(e){
        console.log('Native file support failed to load, trying to reload...');
        if (nativeFileSupport){
            if (nativeFileSupport.parentNode){
                nativeFileSupport.parentNode.removeChild(nativeFileSupport);
            }
            else{
                document.head.removeChild(nativeFileSupport);
            }
        }
        window.setTimeout(addNpapiTag, 0);
    }
}

var addNpapiTag = function(){
    $(document.body).append('<embed type="application/x-npapifileioforchrome" id="pluginId" style="position:absolute; top:0px;left:-10000px;width:5px;height:5px;">');
    window.setTimeout(checkNativeFileSupport, 0);
}
 
checkNativeFileSupport();